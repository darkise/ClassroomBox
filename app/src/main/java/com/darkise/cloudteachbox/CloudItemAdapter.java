package com.darkise.cloudteachbox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import DeviceControlProtocol.FileEntry;

/**
 * Created by Darkise on 2017/8/6.
 */

public class CloudItemAdapter extends BaseAdapter {
    private Context context;

    private List<FileEntry> files = null;

    public CloudItemAdapter(Context context, List<FileEntry> files) {
        this.context = context;
        this.files = files;
    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            // 获得容器
            convertView = LayoutInflater.from(this.context).inflate(R.layout.cloud_item, null);

            // 初始化组件
            viewHolder.image = (ImageView) convertView.findViewById(R.id.clouditem_image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.clouditem_text);
            // 给converHolder附加一个对象
            convertView.setTag(viewHolder);
        } else {
            // 取得converHolder附加的对象
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // 给组件设置资源
        FileEntry picture = files.get(position);
        viewHolder.image.setImageResource(picture.getImage());
        viewHolder.title.setText(picture.getFileName());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return files.get(position);
    }

    public void addEntry(String rfn, String lfn, boolean isDir) {
        FileEntry fileEntry = new FileEntry(rfn, lfn, isDir, new Date(System.currentTimeMillis()));
        files.add(fileEntry);
    }

    public void clearEntries() {
        files.clear();
    }

    class ViewHolder {
        public TextView title;
        public ImageView image;
    }

}
