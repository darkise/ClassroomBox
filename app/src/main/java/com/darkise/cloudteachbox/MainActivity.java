package com.darkise.cloudteachbox;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import DeviceControlProtocol.CloudDiskManager;
import DeviceControlProtocol.TeacherLoginThread;

public class MainActivity extends Activity implements View.OnClickListener {

    private TeacherLoginThread loginThread;

    private LoginDialog loginDialog;

    private int fragmentIndex = -1;
    // 设备标签页，查看、设置设备状态
    private DeviceFragment deviceFragment;
    private View deviceLayout;
    private ImageView deviceImage;
    private TextView deviceText;

    // 云盘服务标签页
    private CloudFragment cloudFragment;
    private View cloudLayout;
    private ImageView cloudImage;
    private TextView cloudText;

    // 通知页面
    private NewsFragment newsFragment;
    private View newsLayout;
    private ImageView newsImage;
    private TextView newsText;

    // 设置标签页
    private SettingsFragment settingsFragment;
    private View settingsLayout;
    private ImageView settingsImage;
    private TextView settingsText;

    /**
     * 用于对Fragment进行管理
     */
    private FragmentManager fragmentManager;

    // 用于线程更新UI
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        fragmentManager = getFragmentManager();

        init_MsgHandler();

        // 启动登陆管理
        loginDialog = new LoginDialog(this);
        //TODO: 测试环境下不打开弹出窗口
        // loginDialog.show();
        loginThread = new TeacherLoginThread(mHandler);
        loginThread.start();

        // 云盘服务
        CloudDiskManager.getInstance().setMsgHandler(mHandler);

        // 默认打开第一页
        setTabSelection(0);
    }

    private void initViews() {
        deviceLayout = findViewById(R.id.device_layout);
        deviceImage = (ImageView) findViewById(R.id.device_image);
        deviceText = (TextView) findViewById(R.id.device_text);
        deviceLayout.setOnClickListener(this);

        cloudLayout = findViewById(R.id.cloud_layout);
        cloudImage = (ImageView) findViewById(R.id.cloud_image);
        cloudText = (TextView) findViewById(R.id.cloud_text);
        cloudLayout.setOnClickListener(this);

        newsLayout = findViewById(R.id.news_layout);
        newsImage = (ImageView) findViewById(R.id.news_image);
        newsText = (TextView) findViewById(R.id.news_text);
        newsLayout.setOnClickListener(this);

        settingsLayout = findViewById(R.id.settings_layout);
        settingsImage = (ImageView) findViewById(R.id.settings_image);
        settingsText = (TextView) findViewById(R.id.settings_text);
        settingsLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.device_layout:
                // 当点击了消息tab时，选中第1个tab
                setTabSelection(0);
                break;
            case R.id.cloud_layout:
                setTabSelection(1);
                break;
            case R.id.news_layout:
                setTabSelection(2);
                break;
            case R.id.settings_layout:
                setTabSelection(3);
                break;
            default:
                break;
        }
    }

    /**
     * 根据传入的index参数来设置选中的tab页。
     *
     * @param index
     *            每个tab页对应的下标。0表示消息，1表示联系人，2表示动态，3表示设置。
     */
    private void setTabSelection(int index) {
        this.fragmentIndex = index;
        // 每次选中之前先清楚掉上次的选中状态
        clearSelection();
        // 开启一个Fragment事务
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
        hideFragments(transaction);
        switch (index) {
            case 0:
                // 当点击了消息tab时，改变控件的图片和文字颜色
                deviceImage.setImageResource(R.drawable.device_selected);
                deviceText.setTextColor(Color.WHITE);
                if (deviceFragment == null) {
                    // 如果DeviceFragment为空，则创建一个并添加到界面上
                    deviceFragment = new DeviceFragment();
                    transaction.add(R.id.content, deviceFragment);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(deviceFragment);
                }
                break;
            case 1:
                // 当点击了消息tab时，改变控件的图片和文字颜色
                cloudImage.setImageResource(R.drawable.cloud_selected);
                cloudText.setTextColor(Color.WHITE);
                if (cloudFragment == null) {
                    // 如果CloudFragment为空，则创建一个并添加到界面上
                    cloudFragment = new CloudFragment();
                    transaction.add(R.id.content, cloudFragment);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(cloudFragment);
                }
                break;
            case 2:
                // 当点击了消息tab时，改变控件的图片和文字颜色
                newsImage.setImageResource(R.drawable.news_selected);
                newsText.setTextColor(Color.WHITE);
                if (newsFragment == null) {
                    // 如果CloudFragment为空，则创建一个并添加到界面上
                    newsFragment = new NewsFragment();
                    transaction.add(R.id.content, newsFragment);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(newsFragment);
                }
                break;
            case 3:
                // 当点击了消息tab时，改变控件的图片和文字颜色
                settingsImage.setImageResource(R.drawable.settings_selected);
                settingsText.setTextColor(Color.WHITE);
                if (settingsFragment == null) {
                    // 如果CloudFragment为空，则创建一个并添加到界面上
                    settingsFragment = new SettingsFragment();
                    transaction.add(R.id.content, settingsFragment);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(settingsFragment);
                }
                break;
            default:
                break;
        }
        transaction.commit();
    }

    /**
     * 清除掉所有的选中状态。
     */
    private void clearSelection() {
        deviceImage.setImageResource(R.drawable.device_unselected);
        deviceText.setTextColor(Color.parseColor("#82858b"));

        cloudImage.setImageResource(R.drawable.cloud_unselected);
        cloudText.setTextColor(Color.parseColor("#82858b"));

        newsImage.setImageResource(R.drawable.news_unselected);
        newsText.setTextColor(Color.parseColor("#82858b"));

        settingsImage.setImageResource(R.drawable.settings_unselected);
        settingsText.setTextColor(Color.parseColor("#82858b"));
    }

    /**
     * 将所有的Fragment都置为隐藏状态。
     * @param transaction
     *            用于对Fragment执行操作的事务
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (deviceFragment != null) {
            transaction.hide(deviceFragment);
        }
        if (cloudFragment != null) {
            transaction.hide(cloudFragment);
        }
        if (newsFragment != null) {
            transaction.hide(newsFragment);
        }
        if (settingsFragment != null) {
            transaction.hide(settingsFragment);
        }
    }

    /**
     * UI更新通知
     */
    private void init_MsgHandler() {
        mHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 0:
                        /**
                         * 登陆消息处理
                         *    显示或取消登陆界面显示
                         *    初始化或清除教师信息
                         */
                        String data = (String) msg.obj;
                        if (data.equals("logout")) {
                            if (cloudFragment != null)
                                cloudFragment.clearFiles();
                            // TODO: 设置显示弹出插卡窗口
                            if (!loginDialog.isShowing())
                                loginDialog.show();
                            //
                        } else if (data.equals("login")) {
                            // TODO: 隐藏插卡提示窗口
                            if (loginDialog.isShowing())
                                loginDialog.hide();
                            if (fragmentIndex == 1 && cloudFragment != null) {
                                cloudFragment.refreshFiles();
                            }
                        }
                        break;
                    case 1:
                        /**
                         * 设备状态消息更新
                         */
                        break;

                    case 2:
                        // 云盘服务
                        break;
                    default:
                        break;
                }
            }

        };
    }
}
