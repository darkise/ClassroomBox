package com.darkise.cloudteachbox;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import DeviceControlProtocol.configures;

public class NewsFragment extends Fragment{
	  
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
            Bundle savedInstanceState) {  
        View newsLayout = inflater.inflate(R.layout.news_layout, container, false);
        WebView webView = (WebView)newsLayout.findViewById(R.id.notice_view);
        // 加载一个默认的通知页
        webView.loadUrl(configures.getInstance().getNoticeUrl());
        return newsLayout;  
    }
  
} 
