package com.darkise.cloudteachbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.TextView;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;


/**
 * Created by Darkise on 2017/8.
 */

public class LoginDialog extends Dialog {
    Activity context;

    String strMsg;
    TextView message;

    public LoginDialog(Activity context) {
        super(context);
        this.context = context;
        super.setTitle("请插卡");
    }

    public LoginDialog(Activity context, int theme) {
        super(context, theme);
        this.context = context;
    }

    public void setTitle(String title) {
        super.setTitle(title);
    }

    public void setMessage(String message) {
        strMsg = message;
        this.message.setText(message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 不显示标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 指定布局
        this.setContentView(R.layout.activity_login);

        this.message = (TextView)this.findViewById(R.id.login_content);

        /*
         * 获取弹出框的窗口对象及参数对象以修改对话框的布局设置, 可以直接调用getWindow(),表示获得这个Activity的Window
         * 对象,这样这可以以同样的方式改变这个Activity的属性.
         */
        Window dialogWindow = this.getWindow();
        WindowManager m = context.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.height = (int) (d.getHeight() * 0.6); // 高度设置为屏幕的0.6
        p.width = (int) (d.getWidth() * 0.8); // 宽度设置为屏幕的0.8
        dialogWindow.setAttributes(p);

        this.setCancelable(false);
    }
}
