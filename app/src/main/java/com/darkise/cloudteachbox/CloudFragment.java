package com.darkise.cloudteachbox;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DeviceControlProtocol.CloudDiskManager;
import DeviceControlProtocol.FileEntry;
import DeviceControlProtocol.utils;

/**
 * Created by Darkise on 2017/8/5.
 */

public class CloudFragment extends Fragment {
    private GridView cloudGridView;
    private CloudItemAdapter cloudAdapter;
    private View cloudLayout = null;
    private CloudDiskManager cloudDiskManager;
    private List<FileEntry> files;

    private int nextState = 0; // 0 无效值， 1 更新目录， 2 打开文件

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // 视图管理
        cloudLayout = inflater.inflate(R.layout.cloud_layout, container, false);
        cloudGridView = (GridView) cloudLayout.findViewById(R.id.gview);

        // 文件类型与现实图标关联
        FileEntry.initSuffix();
        // 文件管理器
        cloudDiskManager = CloudDiskManager.getInstance();

        // 加载目录
        refreshFiles();

        cloudAdapter = new CloudItemAdapter(cloudLayout.getContext(), files);
        cloudGridView.setAdapter(cloudAdapter);

        // 添加GridView的监听点击事件
        cloudGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileEntry file = (FileEntry) cloudAdapter.getItem(position);
                Log.d("CloudTechBox", "点击文件："+file.getFileName());
                // 判断文件是否已经下载
                if (file.getLocalName() == null || file.getPullTime() == null) {
                    // 如果没有下载则调用FTP下载次文件
                    String localName = cloudDiskManager.downloadFile(file.getRemoteName());
                    if (null != localName && localName.length() > 0) {
                        // 下载完成更新节点
                        file.setLocalName(localName);
                        file.setPullTime(new Date(System.currentTimeMillis()));
                    }
                    else {
                        return ;
                    }
                }

                // 调用外部运用开启次文件
                File ofile = new File(file.getLocalName());
                utils.openFileByMIME(cloudLayout.getContext(), ofile);
            }
        });

        return cloudLayout;
    }

    public void clearFiles() {
        files.clear();
        cloudAdapter.notifyDataSetChanged();
    }

    public void noticeUpdate() {
        switch(nextState) {
            case 1:
                // 目录更新
                cloudAdapter.notifyDataSetChanged();
                break;
            case 2:
                // 打开文件
                break;
            case 0:
            default:
        }
    }

    public void refreshFiles() {
        if (null == files) {
            files = new ArrayList<FileEntry>();
        }
        CloudDiskManager.getInstance().list(files);

        // 测试代码
        files.add(new FileEntry("/7895/word.doc", Environment.getExternalStorageDirectory()+"/cloudteachbox/word.doc", false, new Date(System.currentTimeMillis())));
        // 测试代码
    }

}
