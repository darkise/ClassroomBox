package com.darkise.cloudteachbox;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import DeviceControlProtocol.configures;

public class SettingsFragment extends Fragment{
    private View settingsLayout;

    private CheckBox checkBoxPowerInputDelayEn;
    private EditText editTextPowerInputDelays;
    private CheckBox checkBoxPowerOutput0DelayEn;
    private EditText editTextPowerOutput0Delays;
    private CheckBox checkBoxPowerOutput1DelayEn;
    private EditText editTextPowerOutput1Delays;
    private CheckBox checkBoxPowerOutput2DelayEn;
    private EditText editTextPowerOutput2Delays;
    private CheckBox checkBoxPowerOutput3DelayEn;
    private EditText editTextPowerOutput3Delays;
    private CheckBox checkBoxPowerOutput4DelayEn;
    private EditText editTextPowerOutput4Delays;

    private EditText editTextServerHost;
    private EditText editTextServerPort;

    private EditText editTextCloudDiskHost;
    private EditText editTextCloudDiskPort;

    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        settingsLayout = inflater.inflate(R.layout.settings_layout, container, false);
        init_views();
        return settingsLayout;  
    }

    @Override
    public void onPause() {
        configures.getInstance().save();
        super.onPause();
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            Log.d("TAG", "afterTextChanged--------------->");
            try {
                if (checkBoxPowerInputDelayEn.isChecked()) {
                    String str = editTextPowerInputDelays.getText().toString();
                    configures.getInstance().setPowerInputDelays(Integer.parseInt(str));
                }
                if (checkBoxPowerOutput0DelayEn.isChecked()) {
                    String str = editTextPowerOutput0Delays.getText().toString();
                    configures.getInstance().setPowerOutput0Delays(Integer.parseInt(str));
                }
                if (checkBoxPowerOutput1DelayEn.isChecked()) {
                    String str = editTextPowerOutput1Delays.getText().toString();
                    configures.getInstance().setPowerOutput1Delays(Integer.parseInt(str));
                }
                if (checkBoxPowerOutput2DelayEn.isChecked()) {
                    String str = editTextPowerOutput2Delays.getText().toString();
                    configures.getInstance().setPowerOutput2Delays(Integer.parseInt(str));
                }
                if (checkBoxPowerOutput3DelayEn.isChecked()) {
                    String str = editTextPowerOutput3Delays.getText().toString();
                    configures.getInstance().setPowerOutput3Delays(Integer.parseInt(str));
                }
                if (checkBoxPowerOutput4DelayEn.isChecked()) {
                    String str = editTextPowerOutput4Delays.getText().toString();
                    configures.getInstance().setPowerOutput4Delays(Integer.parseInt(str));
                }

                String noticeUrl = ((EditText)settingsLayout.findViewById(R.id.notice_url)).getText().toString();
                configures.getInstance().setNoticeUrl(noticeUrl);

                String svrhost = editTextServerHost.getText().toString();
                String svrport = editTextServerPort.getText().toString();
                configures.getInstance().setServer(svrhost, Integer.parseInt(svrport));

                String cloudhost = editTextCloudDiskHost.getText().toString();
                String cloudport = editTextCloudDiskPort.getText().toString();
                configures.getInstance().setCloudDisk(cloudhost, Integer.parseInt(cloudport));
            }
            catch (Exception e) {
                // 提示错误
                AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(settingsLayout.getContext());
                builder.setTitle("消息").setIcon(android.R.drawable.stat_notify_error);
                builder.setMessage("输入错误");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    private void init_views() {
        checkBoxPowerInputDelayEn = (CheckBox) settingsLayout.findViewById(R.id.power_input_enabled);
        editTextPowerInputDelays = (EditText) settingsLayout.findViewById(R.id.power_input_delays);
        String delaysi = Integer.toString(configures.getInstance().getPowerInputDelays());
        editTextPowerInputDelays.setText(delaysi);
        checkBoxPowerInputDelayEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editTextPowerInputDelays.setEnabled(true);
                } else {
                    editTextPowerInputDelays.setEnabled(false);
                }
            }
        });
        editTextPowerInputDelays.addTextChangedListener(textWatcher);
        if (configures.getInstance().getPowerInputDelays() > 0) {
            checkBoxPowerInputDelayEn.setChecked(true);
            editTextPowerInputDelays.setEnabled(true);
        }
        else {
            checkBoxPowerInputDelayEn.setChecked(false);
            editTextPowerInputDelays.setEnabled(false);
        }

        checkBoxPowerOutput0DelayEn = (CheckBox) settingsLayout.findViewById(R.id.power_output0_enabled);
        editTextPowerOutput0Delays = (EditText) settingsLayout.findViewById(R.id.power_output0_delays);
        String delayO0 = Integer.toString(configures.getInstance().getPowerOutput0Delays());
        editTextPowerOutput0Delays.setText(delayO0);
        checkBoxPowerOutput0DelayEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editTextPowerOutput0Delays.setEnabled(true);
                } else {
                    editTextPowerOutput0Delays.setEnabled(false);
                }
            }
        });
        editTextPowerOutput0Delays.addTextChangedListener(textWatcher);
        if (configures.getInstance().getPowerOutput0Delays() > 0) {
            checkBoxPowerOutput0DelayEn.setChecked(true);
            editTextPowerOutput0Delays.setEnabled(true);
        }
        else {
            checkBoxPowerOutput0DelayEn.setChecked(false);
            editTextPowerOutput0Delays.setEnabled(false);
        }

        checkBoxPowerOutput1DelayEn = (CheckBox) settingsLayout.findViewById(R.id.power_output1_enabled);
        editTextPowerOutput1Delays = (EditText) settingsLayout.findViewById(R.id.power_output1_delays);
        String delayO1 = Integer.toString(configures.getInstance().getPowerOutput1Delays());
        editTextPowerOutput1Delays.setText(delayO1);
        checkBoxPowerOutput1DelayEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editTextPowerOutput1Delays.setEnabled(true);
                } else {
                    editTextPowerOutput1Delays.setEnabled(false);
                }
            }
        });
        editTextPowerOutput1Delays.addTextChangedListener(textWatcher);
        if (configures.getInstance().getPowerOutput1Delays() > 0) {
            checkBoxPowerOutput1DelayEn.setChecked(true);
            editTextPowerOutput1Delays.setEnabled(true);
        }
        else {
            checkBoxPowerOutput1DelayEn.setChecked(false);
            editTextPowerOutput1Delays.setEnabled(false);
        }

        checkBoxPowerOutput2DelayEn = (CheckBox) settingsLayout.findViewById(R.id.power_output2_enabled);
        editTextPowerOutput2Delays = (EditText) settingsLayout.findViewById(R.id.power_output2_delays);
        String delayO2 = Integer.toString(configures.getInstance().getPowerOutput2Delays());
        editTextPowerOutput2Delays.setText(delayO2);
        checkBoxPowerOutput2DelayEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editTextPowerOutput2Delays.setEnabled(true);
                } else {
                    editTextPowerOutput2Delays.setEnabled(false);
                }
            }
        });
        editTextPowerOutput2Delays.addTextChangedListener(textWatcher);
        if (configures.getInstance().getPowerOutput2Delays() > 0) {
            checkBoxPowerOutput2DelayEn.setChecked(true);
            editTextPowerOutput2Delays.setEnabled(true);
        }
        else {
            checkBoxPowerOutput2DelayEn.setChecked(false);
            editTextPowerOutput2Delays.setEnabled(false);
        }

        checkBoxPowerOutput3DelayEn = (CheckBox) settingsLayout.findViewById(R.id.power_output3_enabled);
        editTextPowerOutput3Delays = (EditText) settingsLayout.findViewById(R.id.power_output3_delays);
        String delayO3 = Integer.toString(configures.getInstance().getPowerOutput3Delays());
        editTextPowerOutput3Delays.setText(delayO3);
        checkBoxPowerOutput3DelayEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editTextPowerOutput3Delays.setEnabled(true);
                } else {
                    editTextPowerOutput3Delays.setEnabled(false);
                }
            }
        });
        editTextPowerOutput3Delays.addTextChangedListener(textWatcher);
        if (configures.getInstance().getPowerOutput3Delays() > 0) {
            checkBoxPowerOutput3DelayEn.setChecked(true);
            editTextPowerOutput3Delays.setEnabled(true);
        }
        else {
            checkBoxPowerOutput3DelayEn.setChecked(false);
            editTextPowerOutput3Delays.setEnabled(false);
        }

        checkBoxPowerOutput4DelayEn = (CheckBox) settingsLayout.findViewById(R.id.power_output4_enabled);
        editTextPowerOutput4Delays = (EditText) settingsLayout.findViewById(R.id.power_output4_delays);
        String delayO4 = Integer.toString(configures.getInstance().getPowerOutput4Delays());
        editTextPowerOutput4Delays.setText(delayO4);
        checkBoxPowerOutput4DelayEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editTextPowerOutput4Delays.setEnabled(true);
                } else {
                    editTextPowerOutput4Delays.setEnabled(false);
                }
            }
        });
        editTextPowerOutput4Delays.addTextChangedListener(textWatcher);
        if (configures.getInstance().getPowerOutput4Delays() > 0) {
            checkBoxPowerOutput4DelayEn.setChecked(true);
            editTextPowerOutput4Delays.setEnabled(true);
        }
        else {
            checkBoxPowerOutput4DelayEn.setChecked(false);
            editTextPowerOutput4Delays.setEnabled(false);
        }

        ((EditText)settingsLayout.findViewById(R.id.notice_url)).setText(configures.getInstance().getNoticeUrl());

        editTextServerHost = (EditText) settingsLayout.findViewById(R.id.server_host);
        editTextServerHost.setText(configures.getInstance().getServerHost());
        editTextServerHost.addTextChangedListener(textWatcher);
        editTextServerPort = (EditText) settingsLayout.findViewById(R.id.server_port);
        String svrport = Integer.toString(configures.getInstance().getServerPort());
        editTextServerPort.setText(svrport);
        editTextServerPort.addTextChangedListener(textWatcher);

        editTextCloudDiskHost = (EditText) settingsLayout.findViewById(R.id.clouddisk_host);
        editTextCloudDiskHost.setText(configures.getInstance().getCloudDiskHost());
        editTextCloudDiskHost.addTextChangedListener(textWatcher);
        editTextCloudDiskPort = (EditText) settingsLayout.findViewById(R.id.clouddisk_port);
        String cldiskPort = Integer.toString(configures.getInstance().getCloudDiskPort());
        editTextCloudDiskPort.setText(cldiskPort);
        editTextCloudDiskPort.addTextChangedListener(textWatcher);

        EditText editTextSerialNumber = (EditText) settingsLayout.findViewById(R.id.self_serialnumber);
        editTextSerialNumber.setEnabled(false);
        editTextSerialNumber.setText(configures.getInstance().getSerialNumber());

        EditText editTextHardwareVersion = (EditText) settingsLayout.findViewById(R.id.hardware_version);
        editTextHardwareVersion.setEnabled(false);
        editTextHardwareVersion.setText(configures.getInstance().getHardwareVersion());

        EditText editTextSoftwareVersion = (EditText) settingsLayout.findViewById(R.id.software_version);
        editTextSoftwareVersion.setEnabled(false);
        editTextSoftwareVersion.setText(configures.getInstance().getSoftwareVersion());
    }
} 
