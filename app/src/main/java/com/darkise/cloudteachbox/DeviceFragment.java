package com.darkise.cloudteachbox;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import DeviceControlProtocol.DevicesController;

public class DeviceFragment extends Fragment {
    private View deviceLayout;

    private ImageButton buttonPowerInput;
    private ImageButton buttonPoweroutput0;
    private ImageButton buttonPoweroutput1;
    private ImageButton buttonPoweroutput2;
    private ImageButton buttonPoweroutput3;
    private ImageButton buttonPoweroutput4;

    private ImageButton buttonHdmiInput0;
    private ImageButton buttonHdmiInput1;

    private ImageButton buttonVgaInput0;
    private ImageButton buttonVgaInput1;
    private ImageButton buttonVgaInput2;
    private ImageButton buttonVgaOutput0;
    private ImageButton buttonVgaOutput1;

    private ImageButton buttonAudioInput0;
    private ImageButton buttonAudioInput1;
    private ImageButton buttonAudioOutput0;
    private ImageButton buttonAudioOutput1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        deviceLayout = inflater.inflate(R.layout.device_layout, container, false);
        initView();
        return deviceLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        initListener();
    }

    private void initView() {
        // 电源管理
        buttonPowerInput = (ImageButton)deviceLayout.findViewById(R.id.power_input);
        buttonPoweroutput0 = (ImageButton)deviceLayout.findViewById(R.id.power_output_0);
        buttonPoweroutput1 = (ImageButton)deviceLayout.findViewById(R.id.power_output_1);
        buttonPoweroutput2 = (ImageButton)deviceLayout.findViewById(R.id.power_output_2);
        buttonPoweroutput3 = (ImageButton)deviceLayout.findViewById(R.id.power_output_3);
        buttonPoweroutput4 = (ImageButton)deviceLayout.findViewById(R.id.power_output_4);

        // HDMI切换
        buttonHdmiInput0 = (ImageButton)deviceLayout.findViewById(R.id.hdmi_input_0);
        buttonHdmiInput1 = (ImageButton)deviceLayout.findViewById(R.id.hdmi_input_1);

        // VGA
        buttonVgaInput0 = (ImageButton)deviceLayout.findViewById(R.id.vga_input_0);
        buttonVgaInput1 = (ImageButton)deviceLayout.findViewById(R.id.vga_input_1);
        buttonVgaInput2 = (ImageButton)deviceLayout.findViewById(R.id.vga_input_2);
        buttonVgaOutput0 = (ImageButton)deviceLayout.findViewById(R.id.vga_output_0);
        buttonVgaOutput1 = (ImageButton)deviceLayout.findViewById(R.id.vga_output_1);

        // 音频
        buttonAudioInput0 = (ImageButton)deviceLayout.findViewById(R.id.audio_input_0);
        buttonAudioInput1 = (ImageButton)deviceLayout.findViewById(R.id.audio_input_1);
        buttonAudioOutput0 = (ImageButton)deviceLayout.findViewById(R.id.audio_output_0);
        buttonAudioOutput1 = (ImageButton)deviceLayout.findViewById(R.id.audio_output_1);
    }

    private void initListener() {
        buttonPowerInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setPowerInput();
                // 设置按键状态
                if (DevicesController.getInstance().getPowerInput()) {
                    buttonPowerInput.setImageResource(R.drawable.power_on);
                }
                else {
                    buttonPowerInput.setImageResource(R.drawable.power_off);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输入");
            }
        });
        buttonPoweroutput0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setPowerOutput(0);
                // 设置按键状态
                if (DevicesController.getInstance().getPowerOutput(0)) {
                    buttonPoweroutput0.setImageResource(R.drawable.power_on);
                }
                else {
                    buttonPoweroutput0.setImageResource(R.drawable.power_off);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        buttonPoweroutput1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setPowerOutput(1);
                // 设置按键状态
                if (DevicesController.getInstance().getPowerOutput(1)) {
                    buttonPoweroutput1.setImageResource(R.drawable.power_on);
                }
                else {
                    buttonPoweroutput1.setImageResource(R.drawable.power_off);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        buttonPoweroutput2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setPowerOutput(2);
                // 设置按键状态
                if (DevicesController.getInstance().getPowerOutput(2)) {
                    buttonPoweroutput2.setImageResource(R.drawable.power_on);
                }
                else {
                    buttonPoweroutput2.setImageResource(R.drawable.power_off);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        buttonPoweroutput3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setPowerOutput(3);
                // 设置按键状态
                if (DevicesController.getInstance().getPowerOutput(3)) {
                    buttonPoweroutput3.setImageResource(R.drawable.power_on);
                }
                else {
                    buttonPoweroutput3.setImageResource(R.drawable.power_off);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        buttonPoweroutput4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setPowerOutput(4);
                // 设置按键状态
                if (DevicesController.getInstance().getPowerOutput(4)) {
                    buttonPoweroutput4.setImageResource(R.drawable.power_on);
                }
                else {
                    buttonPoweroutput4.setImageResource(R.drawable.power_off);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        // HDMI 输入
        buttonHdmiInput0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setHdmiInput(0);
                int currState = DevicesController.getInstance().getHdmiInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonHdmiInput0.setImageResource(R.drawable.radio_selected);
                    buttonHdmiInput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonHdmiInput0.setImageResource(R.drawable.radio_unselected);
                    buttonHdmiInput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonHdmiInput0.setImageResource(R.drawable.radio_unselected);
                    buttonHdmiInput1.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        buttonHdmiInput1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setHdmiInput(1);
                int currState = DevicesController.getInstance().getHdmiInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonHdmiInput0.setImageResource(R.drawable.radio_selected);
                    buttonHdmiInput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonHdmiInput0.setImageResource(R.drawable.radio_unselected);
                    buttonHdmiInput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonHdmiInput0.setImageResource(R.drawable.radio_unselected);
                    buttonHdmiInput1.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "电源输出端0");
            }
        });
        // VGA 输入
        buttonVgaInput0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setVGAInput(0);
                int currState = DevicesController.getInstance().getVgaInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_selected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_selected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                else if (2 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "VGA输入0");
            }
        });
        buttonVgaInput1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setVGAInput(1);
                int currState = DevicesController.getInstance().getVgaInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_selected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_selected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                else if (2 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "VGA输入1");
            }
        });
        buttonVgaInput2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setVGAInput(2);
                int currState = DevicesController.getInstance().getVgaInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_selected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_selected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                else if (2 == currState) {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonVgaInput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput1.setImageResource(R.drawable.radio_unselected);
                    buttonVgaInput2.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "VGA输入2");
            }
        });
        // VGA 输出
        buttonVgaOutput0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setVGAOutput(0);
                int currState = DevicesController.getInstance().getVgaOutput();
                // 设置按键状态
                if (0 == currState) {
                    buttonVgaOutput0.setImageResource(R.drawable.radio_selected);
                    buttonVgaOutput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonVgaOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaOutput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonVgaOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaOutput1.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "VGA输出0");
            }
        });
        buttonVgaOutput1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setVGAOutput(1);
                int currState = DevicesController.getInstance().getVgaOutput();
                // 设置按键状态
                if (0 == currState) {
                    buttonVgaOutput0.setImageResource(R.drawable.radio_selected);
                    buttonVgaOutput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonVgaOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaOutput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonVgaOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonVgaOutput1.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "VGA输出1");
            }
        });
        // 音频输入
        buttonAudioInput0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setAudioInput(0);
                int currState = DevicesController.getInstance().getAudioInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonAudioInput0.setImageResource(R.drawable.radio_selected);
                    buttonAudioInput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonAudioInput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioInput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonAudioInput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioInput1.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "音频输入0");
            }
        });
        buttonAudioInput1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setAudioInput(1);
                int currState = DevicesController.getInstance().getAudioInput();
                // 设置按键状态
                if (0 == currState) {
                    buttonAudioInput0.setImageResource(R.drawable.radio_selected);
                    buttonAudioInput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonAudioInput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioInput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonAudioInput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioInput1.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "音频输入1");
            }
        });
        // 音频输出
        buttonAudioOutput0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setAudioOutput(0);
                int currState = DevicesController.getInstance().getAudioOutput();
                // 设置按键状态
                if (0 == currState) {
                    buttonAudioOutput0.setImageResource(R.drawable.radio_selected);
                    buttonAudioOutput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonAudioOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioOutput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonAudioOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioOutput0.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "音频输出0");
            }
        });
        buttonAudioOutput1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicesController.getInstance().setAudioOutput(1);
                int currState = DevicesController.getInstance().getAudioOutput();
                // 设置按键状态
                if (0 == currState) {
                    buttonAudioOutput0.setImageResource(R.drawable.radio_selected);
                    buttonAudioOutput1.setImageResource(R.drawable.radio_unselected);
                }
                else if (1 == currState) {
                    buttonAudioOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioOutput1.setImageResource(R.drawable.radio_selected);
                }
                else {
                    buttonAudioOutput0.setImageResource(R.drawable.radio_unselected);
                    buttonAudioOutput0.setImageResource(R.drawable.radio_unselected);
                }
                // TODO Auto-generated method stub
                Log.d("CloudTeachBox", "音频输出1");
            }
        });

        // WiFi display使能
    }
}