package com.darkise.cloudteachbox;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.Log;

import DeviceControlProtocol.UdpManagerThread;

public class CloudTeachBoxService extends Service {
    public CloudTeachBoxService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    public void onCreate() {
        super.onCreate();

        // 设置前台服务，在API11之后构建Notification的方式
                                                           // 获取一个Notification构造器
        Notification.Builder builder = new Notification.Builder(this.getApplicationContext());
        Intent nfIntent = new Intent(this, MainActivity.class);
        builder.setContentIntent(PendingIntent
                .getActivity(this, 0, nfIntent, 0))         // 设置PendingIntent
                                                            // 设置下拉列表中的图标(大图标)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setContentTitle("云教育平台")               // 设置下拉列表里的标题
                .setSmallIcon(R.mipmap.ic_launcher)         // 设置状态栏内的小图标
                .setContentText("网络服务")                  // 设置上下文内容
                .setWhen(System.currentTimeMillis());       // 设置该通知发生的时间
        Notification notification = builder.build();        // 获取构建好的Notification
        notification.defaults = Notification.DEFAULT_SOUND; //设置为默认的声音
        startForeground(110, notification);                 // 开始前台服务

        Log.d("CloudTechBox", "Service created.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("CloudTechBox", "onStartCommand() executed");
        // 创建网络服务线程
        UdpManagerThread udpThread = new UdpManagerThread();
        udpThread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);// 停止前台服务--参数：表示是否移除之前的通知
        Log.d("CloudTechBox", "Service quit!");
    }
}
