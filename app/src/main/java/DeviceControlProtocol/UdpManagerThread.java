package DeviceControlProtocol;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Darkise on 2017/7.
 */

public class UdpManagerThread extends Thread {

    private boolean isRunning = true;
    private DatagramSocket broadcastSocket = null;
    private DatagramSocket unicastSocket = null;
    private static Queue<DatagramPacket> sndQueue = null;
    private static InetAddress serverAddr = null;
    private long sndTime = 0;

    public UdpManagerThread() {
        super();
    }
    
    @Override
    public void run() {
        byte[] by = new byte[1024];
        //
        while (isRunning) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            broadcastRoutine();

            uniReceiveRoutine();

            uniSendRoutine();
        }
    }

    public void stopRunning() {
        isRunning = false;
    }

    private void broadcastRoutine() {
        byte[] by = new byte[1024];
        try {
            // Broadcast
            if (broadcastSocket == null) {
                broadcastSocket = new DatagramSocket(null);
                broadcastSocket.setReuseAddress(true);
                broadcastSocket.setSoTimeout(100);
                broadcastSocket.bind(new InetSocketAddress(configures.getInstance().getBroadcastPort()));
            }
            // Receiving
            DatagramPacket packet = new DatagramPacket(by, by.length);
            broadcastSocket.receive(packet);
            // Debug output
            String str = new String(packet.getData(), 0, packet.getLength());
            System.out.println("Broadcast接收到的数据为：" + str);
            Log.v("CloudTechBox", "Broadcast已获取服务器端发过来的数据。。。。。" + str);
            // Parsing
            serverAddr = packet.getAddress();
            DCPBroadcast bst = new DCPBroadcast(packet.getData());
            if (bst.decode()) {
                // 解析成功，通过TCP socket回应
                DCPBroadcast resp = new DCPBroadcast();
                udpSend(resp.encode());
            } else {
                // 解析失败丢弃此包
                Log.v("CloudTechBox", "无法解析广播包");
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uniReceiveRoutine() {
        byte[] by = new byte[1024];
        try {
            // Unicast
            if (unicastSocket == null) {
                unicastSocket = new DatagramSocket(null);
                unicastSocket.setReuseAddress(true);
                unicastSocket.setSoTimeout(100);
                unicastSocket.bind(new InetSocketAddress(configures.getInstance().getLocalPort()));
                sndTime = System.currentTimeMillis();
            }
            // Receiving
            DatagramPacket packet = new DatagramPacket(by, by.length);
            unicastSocket.receive(packet);
            // Debug output
            String str = new String(packet.getData(), 0, packet.getLength());
            System.out.println("UDP接收到的数据为：" + str);
            Log.v("CloudTechBox", "UDP已获取服务器端发过来的数据。。。。。" + str);
            // 数据处理
            Deal(packet.getData());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uniSendRoutine() {
        if (null == sndQueue) {
            sndQueue = new LinkedList<DatagramPacket>();
        }
        while(sndQueue.size() > 0) {
            // Get an entry
            DatagramPacket packet = sndQueue.element();
            // Send it out
            try {
                if (unicastSocket == null) {
                    unicastSocket = new DatagramSocket(null);
                    unicastSocket.setReuseAddress(true);
                    unicastSocket.setSoTimeout(100);
                    unicastSocket.bind(new InetSocketAddress(configures.getInstance().getBroadcastPort()));
                    sndTime = System.currentTimeMillis();
                }
                unicastSocket.send(packet);
                sndQueue.remove();
                sndTime = System.currentTimeMillis();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /* Send heartbeat */
            if (System.currentTimeMillis() - sndTime >= configures.getInstance().getHeartBeatInterval()) {
                DCPHartbeat hb = new DCPHartbeat();
                DatagramPacket hbPacket = new DatagramPacket(hb.encode(), hb.encode().length, serverAddr, configures.getInstance().getServerPort());
                try {
                    unicastSocket.send(hbPacket);
                    sndTime = System.currentTimeMillis();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void udpSend(byte[] data) {
        if (null == sndQueue) {
            sndQueue = new LinkedList<DatagramPacket>();
        }
        if (null == serverAddr) {
            try {
                serverAddr = InetAddress.getByName(configures.getInstance().getServerHost());
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        DatagramPacket packet = new DatagramPacket(data, data.length, serverAddr, configures.getInstance().getServerPort());
        sndQueue.add(packet);
    }

    private void Deal(byte[] input) {
        if (input.length < 3) {
            Log.d("CloudTechBox", "指令长度错误");
            return ;
        }
        switch (input[2]) {
            case DCP.PacketTypeCommand:
                DCPCommand cmd = new DCPCommand(input);
                cmd.decode();
                break;
            case DCP.PacketTypeDeviceState:
                DCPDeviceState state = new DCPDeviceState(input);
                state.decode();
                break;
            case DCP.PacketTypeHartbeat:
                DCPHartbeat hb = new DCPHartbeat(input);
                hb.decode();
                break;
            default:
                Log.d("CloudTechBox", "未知指令");
        }
    }
}
