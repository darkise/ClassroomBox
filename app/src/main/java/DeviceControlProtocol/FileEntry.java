package DeviceControlProtocol;

import com.darkise.cloudteachbox.R;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Darkise on 2017/8/7.
 */

public class FileEntry {
    private static Map<String, Object> suffixes = null;

    String fileName;   // 显示文件名
    String remoteName; // 云端文件名，全路径
    String localName;  // 本地文件名，全路径
    boolean isDir;     // 是否是目录
    int image;         // 显示图标
    Date pullTime;     // 下载时间
    Date pushTime;     // 上传时间
    boolean modified;  // 是否被修改过

    public FileEntry() {
        isDir = isDir;
        modified = false;
        localName = null;
        pullTime = null;
        pushTime = null;
    }

    public FileEntry(String rfn, boolean isDir) {
        this.isDir = isDir;
        modified = false;
        localName = null;
        pullTime = null;
        pushTime = null;

        decode();
    }

    public FileEntry(String rfn, String lfn, boolean isDir, Date date) {
        this.isDir = isDir;
        modified = false;
        remoteName = rfn;
        localName = lfn;
        pullTime = date;

        decode();
    }

    public void setDownload(String remoteName, Date pullTime) {
        this.remoteName = remoteName;
        this.pullTime = pullTime;
    }

    public void setUpload(Date pushTime) {
        this.pushTime = pushTime;
    }

    private void decode() {
        String[] paths = remoteName.split("/");
        if (paths.length < 1) {
            return ;
        }
        fileName = paths[paths.length - 1];
        //
        if (this.isDir) {
            image = R.drawable.dir;
        }
        else {
            String[] splits = fileName.split("\\.");
            if (splits.length < 2) {
                image = R.drawable.help;
            }
            else {
                String suffix = splits[splits.length - 1];
                suffix.toLowerCase();
                if (null == suffixes || null == suffixes.get(suffix)) {
                    image = R.drawable.help;
                } else {
                    image = (int) suffixes.get(suffix);
                }
            }
        }
    }
    public String getFileName() {
        return fileName;
    }
    public int getImage() {
        return image;
    }
    public  String getRemoteName() {return remoteName; }
    public String getLocalName() { return localName; }
    public Date getPullTime() { return pullTime; }
    public Date getPushTime() { return pushTime; }

    public void setLocalName(String localName) { this.localName = localName; }
    public void setPullTime(Date d) {this.pullTime = d; }
    public void setModified() {
        modified = true;
    }
    public void setPushTime(Date date) {
        pushTime = date;
    }

    public static void initSuffix() {
        /* 只进行一次初始化 */
        if (null ==  suffixes) {
            suffixes = new HashMap<String, Object>();
            suffixes.put("doc", R.drawable.word);
            suffixes.put("docx", R.drawable.word);
            suffixes.put("ppt", R.drawable.ppt);
            suffixes.put("pptx", R.drawable.ppt);
            suffixes.put("xls", R.drawable.excel);
            suffixes.put("xlsx", R.drawable.excel);
            suffixes.put("pdf", R.drawable.pdf);
            suffixes.put("mp3", R.drawable.mp4);
            suffixes.put("mp4", R.drawable.mp4);
        }
    }
}
