package DeviceControlProtocol;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by Darkise on 2017/7/16.
 */

public class DCPBroadcast extends DCP {
    public static final Parcelable.Creator<DCPBroadcast> CREATOR = new Parcelable.Creator<DCPBroadcast>() {

        public DCPBroadcast createFromParcel(Parcel in) {
            return new DCPBroadcast(in);
        }

        public DCPBroadcast[] newArray(int size) {
            return new DCPBroadcast[size];
        }
    };
    private DCPBroadcast(Parcel in) {
        super(in);
    }

    public static final byte commandLENGTH = (byte)12;  // Message to server
    public static final byte responseLENGTH = (byte)3;  // Message from server
    public DCPBroadcast() {
        super(commandLENGTH, DCP.PacketTypeBroadcast);
        super.Data[7] = 0x04;
        getHostIP();
    }

    public DCPBroadcast(byte[] data) {
        super(data);
    }

    @Override
    public boolean decode(){
        if (null == super.Data || responseLENGTH != super.Data[0]
                || DCP.VERSION != super.Data[1] || DCP.PacketTypeBroadcast != super.Data[2])
            return false;

        return true;
    }

    private void getHostIP() {
        int ip = utils.getHostIP();
        if (ip != utils.INVALID_IPV4) {
            super.Data[8] = (byte) ((ip >> 24) & 0xff);
            super.Data[8] = (byte) ((ip >> 16) & 0xff);
            super.Data[8] = (byte) ((ip >> 8) & 0xff);
            super.Data[8] = (byte) (ip & 0xff);
        }
    }
}
