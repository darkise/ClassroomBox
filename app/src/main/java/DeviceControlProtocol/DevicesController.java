package DeviceControlProtocol;

/**
 * Created by Darkise on 2017/8.
 */

public class DevicesController {
    private static final DevicesController ourInstance = new DevicesController();

    private boolean powerInput = false;
    private boolean[] powerOutput = {false, false, false, false, false};
    private float powerInputVoltage;          // 输入电压
    private float powerInputElectricity;      // 输入电流
    private float[] powerOutputVoltage;       // 输出电压
    private float[] powerOutputElectricity;   // 输出电流

    private int hdmiInput;
    private int hdmiOutput;

    private int vgaInput;
    private int vgaOutput;

    private int audioInput;
    private int audioOutput;

    private boolean wifiDisplay = false;

    public static DevicesController getInstance() {
        return ourInstance;
    }

    private DevicesController() {

    }

    public int getHdmiInput() {
        return hdmiInput;
    }
    public void setHdmiInput(int hdmi) {
        if (hdmiInput == hdmi) {
            return ;
        }
        hdmiInput = hdmi;
        // TODO: 执行HDMI输入端切换操作
    }

    public int getHdmiOutput() {
        return hdmiOutput;
    }
    public void setHDMIOutput(int hdmi) {
        return ;
    }

    public int getVgaInput() {
        return vgaInput;
    }
    public void setVGAInput(int vga) {
        if (vgaInput == vga) {
            return ;
        }
        vgaInput = vga;
        // TODO: 执行VGA输入切换
    }

    public int getVgaOutput() {
        return vgaOutput;
    }
    public void setVGAOutput(int vga) {
        if (vgaOutput == vga) {
            return ;
        }
        vgaOutput = vga;
        // TODO: 执行VGA输出切换
    }

    public boolean getPowerInput() {
        return powerInput;
    }
    public void setPowerInput() {
        powerInput = !powerInput;
        // TODO:
    }

    public boolean[] getPowerOutput() {
        return powerOutput;
    }
    public boolean getPowerOutput(int power) { return powerOutput[power]; }
    public void setPowerOutput(int power) {
        powerOutput[power] = !powerOutput[power];
        // TODO: 电源操作
    }

    public int getAudioInput() { return audioInput; }
    public void setAudioInput(int audio) {
        if (audioInput == audio) {
            return ;
        }
        audioInput = audio;
        // TODO: 音频输入切换
    }

    public int getAudioOutput() { return audioOutput; }
    public void setAudioOutput(int audio) {
        if (audioOutput == audio) {
            return ;
        }
        audioOutput = audio;
        // TODO: 音频输入切换
    }

    public void updatePowerStates() {
        // TODO: 通过外部接口获取电源模块状态，包括当前的开关电压电流状态
    }

    public void updateHDMIStates() {
        // TODO: 通过外部接口获取
    }

    public void updateVGAStates() {
        // TODO: 通过外部接口获取
    }

    public void updateAudioStates() {
        // TODO: 通过外部接口获取
    }

    public void wifiDisplayEnable(boolean enable) {
        if (!wifiDisplay && enable) {
            // TODO: 使能WIFI display功能
        }
        else if (wifiDisplay && !enable) {
            // TODO: 关闭WIFI display功能
        }

        wifiDisplay = enable;
    }
}
