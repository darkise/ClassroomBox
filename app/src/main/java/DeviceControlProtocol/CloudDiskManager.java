package DeviceControlProtocol;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import it.sauronsoftware.ftp4j.FTPFile;

/**
 * Created by Darkise on 2017/8/6.
 */

public final class CloudDiskManager {
    private static CloudDiskManager ourInstance = new CloudDiskManager();

    public static CloudDiskManager getInstance() {
        return ourInstance;
    }

    public String userName;
    public String userPwd;

    private String cloudDiskHost;
    private int cloudDiskPort;
    private FTPClient ftpClient;

    private List<FileEntry> fileList;
    private String remotePath;
    private String remoteFile;
    private String localFile;

    private Handler msgHandler;
    private Thread runThread;

    private CloudDiskManager() {
        cloudDiskHost = configures.getInstance().getCloudDiskHost();
        cloudDiskPort = configures.getInstance().getCloudDiskPort();
        ftpClient = new FTPClient();
    }

    public void setMsgHandler(Handler handler) {
        msgHandler = handler;
    }

    public void setCloudDiskServer(String host, int port) {cloudDiskHost = host; cloudDiskPort = port; }
    public void setCloudDiskHost(String host) {
        cloudDiskHost = host;
    }
    public void setCloudDiskPort(int port) {
        cloudDiskPort = port;
    }

    public void setUser(String userName, String userPwd) {
        userName = userName;
        userPwd = userPwd;
    }

    private boolean connected() {
        try {
            if (!ftpClient.isConnected() &&
                    cloudDiskHost != null && cloudDiskHost.length() > 0 &&
                    cloudDiskPort > 0 && cloudDiskPort < 0xffff) {
                ftpClient.connect(cloudDiskHost, cloudDiskPort);
            }

            if (null == userName || null == userPwd)
                return false;

            if (ftpClient.isConnected()) {
                ftpClient.login(userName, userPwd);
            }
            else {
                return false;
            }

            return true;
        } catch (it.sauronsoftware.ftp4j.FTPIllegalReplyException e) {
            e.printStackTrace();
        } catch (it.sauronsoftware.ftp4j.FTPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void logout() {
        try {
            if (ftpClient != null)
                ftpClient.logout();
        } catch (it.sauronsoftware.ftp4j.FTPException e) {
            e.printStackTrace();
        } catch (it.sauronsoftware.ftp4j.FTPIllegalReplyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public List<FileEntry> list(List<FileEntry> files) {
        fileList = files;
        if (runThread != null) {
            return null;
        }
        runThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!connected()) {
                        return;
                    }
                    FTPFile[] flist = ftpClient.list();
                    for (int i = 0; i < flist.length; i++) {
                        fileList.add(new FileEntry(flist[i].getName(), (FTPFile.TYPE_DIRECTORY == flist[i].getType())));
                    }
                    /*Message m = msgHandler.obtainMessage();
                    m.what = 2;
                    msgHandler.sendMessage(m);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        runThread.start();
        // 等待过程完成
        try {
            runThread.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        runThread = null;

        return fileList;
    }

    public void changePath(String path) {
        remotePath = path;
        if (runThread != null) {
            return;
        }
        runThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (!connected()) {
                    return ;
                }
                try {
                    ftpClient.changeDirectory(remotePath);
                } catch (it.sauronsoftware.ftp4j.FTPException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPIllegalReplyException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        runThread.start();
        // 等待过程完成
        try {
            runThread.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        runThread = null;
    }

    public void changePathBack() {
        if (runThread != null) {
            return;
        }
        runThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (!connected()) {
                    return ;
                }
                try {
                    ftpClient.changeDirectoryUp();
                } catch (it.sauronsoftware.ftp4j.FTPException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPIllegalReplyException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        runThread.start();
        // 等待过程完成
        try {
            runThread.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        runThread = null;
    }

    public String downloadFile(String rf) {
        remoteFile = rf;
        localFile = configures.getInstance().getCloudDiskCacheDir() + "/" + userName + "/";
        if (runThread != null) {
            return null;
        }
        runThread = new Thread(new Runnable() {
            @Override
            public void run() {
                // 确定本地文件名

                String[] paths = remotePath.split("/");
                if (paths.length < 1) {
                    localFile = null;
                    return ;
                }
                localFile += paths[paths.length - 1];
                // 下载文件
                if (!connected()) {
                    localFile = null;
                    return ;
                }
                try {
                    ftpClient.download(localFile, new java.io.File(remoteFile));
                } catch (it.sauronsoftware.ftp4j.FTPDataTransferException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPAbortedException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPIllegalReplyException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        runThread.start();
        // 等待过程完成
        try {
            runThread.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        runThread = null;

        return localFile;
    }

    public void uploadFile(String lf) {
        localFile = lf;
        if (runThread != null) {
            return ;
        }
        runThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (!connected()) {
                    return ;
                }
                try {
                    ftpClient.upload(new java.io.File(localFile));
                } catch (it.sauronsoftware.ftp4j.FTPDataTransferException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPAbortedException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPException e) {
                    e.printStackTrace();
                } catch (it.sauronsoftware.ftp4j.FTPIllegalReplyException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        runThread.start();
        // 等待过程完成
        try {
            runThread.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        runThread = null;
    }
/*
    private class MyTransferListener implements FTPDataTransferListener {

        public void started() {
            // Transfer started
        }

        public void transferred(int length) {
            // Yet other length bytes has been transferred since the last time this
            // method was called
        }

        public void completed() {
            // Transfer completed
        }

        public void aborted() {
            // Transfer aborted
        }

        public void failed() {
            // Transfer failed
        }

    }*/
}
