package DeviceControlProtocol;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Darkise on 2017/7/16.
 */

public class DCPDeviceState extends DCP {
    public static final Parcelable.Creator<DCPDeviceState> CREATOR = new Parcelable.Creator<DCPDeviceState>() {

        public DCPDeviceState createFromParcel(Parcel in) {
            return new DCPDeviceState(in);
        }

        public DCPDeviceState[] newArray(int size) {
            return new DCPDeviceState[size];
        }
    };
    private DCPDeviceState(Parcel in) {
        super(in);
    }

    private static final byte commandLENGTH = (byte)13;  // Message to server
    private static final byte responseLENGTH = (byte)9;  // Message from server

    public DCPDeviceState(byte device, byte state) {
        super(commandLENGTH, DCP.PacketTypeDeviceState);
        super.Data[7] = 0x05;
        super.Data[8] = 0x04;
        super.Data[9] = device;
        super.Data[10] = state;
        super.Data[10] = (byte)0xff;
        super.Data[11] = (byte)0xff;
    }

    public DCPDeviceState(byte[] data) {
        super(data);
    }

    private byte result;
    @Override
    public boolean decode() {
        if (null == super.Data || responseLENGTH != super.Data[0]
                || DCP.VERSION != super.Data[1] || DCP.PacketTypeDeviceState != super.Data[2])
            return false;

        result = super.Data[8];

        return true;
    }

    public byte getResult() {
        return result;
    }
}
