package DeviceControlProtocol;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Darkise on 2017/7/16.
 */

public class DCPCommand extends DCP {
    public static final Parcelable.Creator<DCPCommand> CREATOR = new Parcelable.Creator<DCPCommand>() {

        public DCPCommand createFromParcel(Parcel in) {
            return new DCPCommand(in);
        }

        public DCPCommand[] newArray(int size) {
            return new DCPCommand[size];
        }
    };
    private DCPCommand(Parcel in) {
        super(in);
    }

    private static final byte commandLENGTH = (byte)9;  // Message to server
    private static final byte responseLENGTH = (byte)13;  // Message from server

    public DCPCommand() {
        super(commandLENGTH, DCP.PacketTypeCommand);
        super.Data[7] = 0x01;
        super.Data[8] = 0;
    }

    public DCPCommand(byte result) {
        super((byte)9, DCP.PacketTypeCommand);
        super.Data[7] = 0x01;
        super.Data[8] = result;
    }

    public DCPCommand(byte[] data) {
        super(data);
    }

    /* Functions for parse command from server */
    private byte DeviceType;
    private byte CommandType;
    private byte[] Actions;
    public boolean decode() {
        if (null == super.Data || responseLENGTH != super.Data[0]
                || DCP.VERSION != super.Data[1] || DCP.PacketTypeCommand != super.Data[2])
            return false;

        //
        DeviceType = super.Data[9];
        CommandType = super.Data[10];
        Actions = new byte[super.Data[8]-2];
        System.arraycopy(super.Data, 11, Actions, 0, super.Data[8] - 2);

        return true;
    }

    public byte getDeviceType() {
        return DeviceType;
    }

    public  byte getCommandType() {
        return CommandType;
    }

    public byte[] getActions() {
        return Actions;
    }
}
