package DeviceControlProtocol;

/**
 * Created by Darkise on 2017/8
 * 教师卡读取封装
 * 目前假设：
 * 1. 卡内只存放一个ID，ID和教师身份绑定由后台服务完成，与此程序无关
 * 2. 在没有教师卡的情况下不可进入应用
 * 3. 后台服务必须提供一个校验教师卡的接口，其接收教师卡ID返回教师相关信息
 */

public final class TeacherCardReader {

    public TeacherCardReader() {}

    // 读取教师卡信息，卡内只存储一个身份ID
    public static byte[] getCardID() {
        // 测试代码begin
        byte[] ret = {1,3,5,6,8,0,4,9,2};
        return ret;
        // 测试代码end
    }
}
