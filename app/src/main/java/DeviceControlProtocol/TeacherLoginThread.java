package DeviceControlProtocol;

import android.os.Handler;
import android.os.Message;

/**
 * Created by Darkise on 2017/8.
 * 管理教师卡系统
 * 未登陆情况下，
 *    保持登陆界面始终保持最前段
 *    尝试读取IC卡信息，并将信息发送到服务器
 *    服务器需要验证信息并返回教师相关信息：云盘接入信息
 * 在已登陆情况下，
 *    关闭登陆界面
 *    更新教师信息
 * 发现拔卡后，
 *    开启登陆界面
 *    上传保存的修改文件
 *    清除教师信息：内存信息，下载文件
 */

public class TeacherLoginThread extends Thread {
    private String teacherID;
    private String teacherName;
    private String cloudName;
    private String cloudPwd;

    public static final int LOGIN_STATE_OFF = 0;
    public static final int LOGIN_STATE_WAITING = 1;
    public static final int LOGIN_STATE_DONE = 2;
    private int loginState = LOGIN_STATE_OFF;

    private long activeTime = 0;

    //
    private Handler handler;

    public TeacherLoginThread(Handler handler) {
        super();
        this.handler = handler;
    }

    @Override
    public void run() {
        while(true) {
            // 尝试读取教师卡
            byte[] id = TeacherCardReader.getCardID();
            if (null == id || id.length < 0) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 如果没有教师卡，则检查是否需要清除本地数据
                logout();
                continue;
            }

            // 如果检测到教师卡，则检测上线状态
            if (LOGIN_STATE_DONE == loginState) {

            }
            else if (LOGIN_STATE_WAITING == loginState) {
                // 已发送请求到服务器，等待回应

            }
            else if (LOGIN_STATE_OFF == loginState) {
                // 如果未上线，则尝试发送教师上线信息到服务器
                // 更新状态
                activeTime = System.currentTimeMillis();
                loginState = LOGIN_STATE_WAITING;
            }
            // 服务器连接维护，使用阻塞式socket
            // 检查连接状态，未连接则尝试连接
            // 如果已连接，则尝试读取服务器信息
            // 如果服务器有回应，则解析回应
            // 获取教师显示名，云端设置（用户名密码，共享路径等）
        }
    }

    public void logined() {
        CloudDiskManager.getInstance().setUser(cloudName, cloudPwd);
        loginState = LOGIN_STATE_DONE;
        // Send message to main thread
        handler.sendEmptyMessage(0);
        Message msg =new Message();
        msg.obj = "login";
        handler.sendMessage(msg);
    }

    public void logout() {
        loginState = LOGIN_STATE_OFF;
        // Send message to main thread
        // 上传已修改文件

        CloudDiskManager.getInstance().setUser(null, null);
        CloudDiskManager.getInstance().logout();

        // UI更新
        handler.sendEmptyMessage(0);
        Message msg =new Message();
        msg.obj = "logout";
        handler.sendMessage(msg);

        // 清除已下载文件，直接删除目录
        String localPath = configures.getInstance().getCloudDiskCacheDir() + "/" + cloudName;
        utils.deleteDir(localPath);

        // 运行环境
        teacherID = null;
        teacherName = null;
        cloudName = null;
        cloudPwd = null;
    }
}
