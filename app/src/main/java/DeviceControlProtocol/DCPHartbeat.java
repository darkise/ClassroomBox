package DeviceControlProtocol;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Darkise on 2017/7/16.
 */

public class DCPHartbeat extends DCP {
    public static final Parcelable.Creator<DCPHartbeat> CREATOR = new Parcelable.Creator<DCPHartbeat>() {

        public DCPHartbeat createFromParcel(Parcel in) {
            return new DCPHartbeat(in);
        }

        public DCPHartbeat[] newArray(int size) {
            return new DCPHartbeat[size];
        }
    };
    private DCPHartbeat(Parcel in) {
        super(in);
    }

    public static final byte commandLENGTH = (byte)9;     // Message to server
    public static final byte responseLENGTH = (byte)15;   // Message from server

    private int year;
    private int mouth;
    private int day;
    private int hour;
    private int minute;
    private int second;
    /**/
    public DCPHartbeat() {
        super(DCP.PacketTypeHartbeat, commandLENGTH);
        super.Data[7] = (byte)0x01;
        super.Data[8] = getDeviceState();
    }

    public DCPHartbeat(byte[] data) {
        super(data);
    }

    private byte getDeviceState() {
        return 0x01;
    }

    @Override
    public boolean decode() {

        if (null == super.Data || responseLENGTH != super.Data[0]
                || DCP.VERSION != super.Data[1] || DCP.PacketTypeHartbeat != super.Data[2])
            return false;
        //
        year = super.Data[8] >> 8 | super.Data[9];
        mouth = super.Data[10];
        day = super.Data[11];
        hour = super.Data[12];
        minute = super.Data[13];
        second = super.Data[14];

        return true;
    }
}
