package DeviceControlProtocol;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Created by Darkise on 2017/7/16.
 */

public abstract class DCP  implements Parcelable {
    /* 版本号 */
    public static final byte VERSION = 0x01;

    /* 设备类型 */
    public static final byte DeviceTypeController = 0x00;
    public static final byte DeviceTypePC = 0x01;
    public static final byte DeviceTypeProjector = 0x02;
    public static final byte DeviceTypeOneInBox = 0x03;
    public static final byte DeviceTypeCamara = 0x04;
    public static final byte DeviceTypeICReader = 0x05;
    public static final byte DeviceTypeVideoEX = 0x06;

    /* 包类型 */
    public static final byte PacketTypeHartbeat = 0x01;
    public static final byte PacketTypeDeviceState = 0x02;
    public static final byte PacketTypeCommand = 0x03;
    public static final byte PacketTypeBroadcast = 0x04;

    /* 设备状态 */
    public static final byte DeviceStateStayOff = 0x00;
    public static final byte DeviceStateNormal = 0x01;
    public static final byte DeviceStateError = 0x02;

    /* 命令执行状态 */
    public static final byte ResultOK = 0x00;
    public static final byte ResultFail = 0x01;

    /* 变量 */
    protected byte[] Data;
    private int BoxID;

    public DCP(byte command) {
        this(command,(byte)0x08);
        Data[7] = 0x00;
    }

    public DCP(byte commandType, byte length) {
        BoxID = utils.getDeviceID();
        Data = new byte[length];
        Data[0] = length;
        Data[1] = VERSION;
        Data[2] = commandType;
        Data[3] = (byte)(BoxID & 0x0ff);
        Data[4] = (byte)((BoxID >> 8) & 0x0ff);
        Data[5] = (byte)((BoxID >> 16) & 0x0ff);
        Data[6] = (byte)((BoxID >> 24) & 0x0ff);
    }

    public DCP(byte[] data) {
        Data = data;
    }

    public byte[] encode() {
        return Data;
    }

    public DCP(Parcel in) {
        readFromParcel(in);
    }

    public boolean decode() {
        return false;
    }

    @Override
    public String toString() {
        return "C." + /*getId().toString() + "." + */Integer.toString(Data[2]) + " - " + Arrays.toString(Data);
    }

    /*
    *  Parcelable override
    * */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeByteArray(Data);
    }

    private void readFromParcel(Parcel in) {
        Data = in.createByteArray();
    }
}
