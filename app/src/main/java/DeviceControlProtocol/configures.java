package DeviceControlProtocol;

/**
 * Created by Darkise on 2017/8.
 * 用于统一管理配置文件
 */

public class configures {
    private static final String SerialSeed = "darkiseproductions";
    private static final configures ourInstance = new configures();

    public static configures getInstance() {
        return ourInstance;
    }

    public String Configure_FileNanme = "/sdcard/cloudteachbox/etc/configure.xml";

    private int powerInputDelays = 0;
    private int powerOutput0Delays = 0;
    private int powerOutput1Delays = 0;
    private int powerOutput2Delays = 0;
    private int powerOutput3Delays = 0;
    private int powerOutput4Delays = 0;

    private String noticeUrl = "http://news.baidu.com";

    /**
     * 使用UDP通讯机制，
     * 收包规则： 1. 监听广播端口，此端口收到的报文为服务器发出的广播报文
     *          2. 监听本地端口， 此端口接收的报文为服务器发出的数据报文
     * 发包规则： 无论收到的广播报文还是数据报文，本程序回应的UDP报文目标都是服务器地址+服务器端口，本地端口为本地端口
     * */
    private String serverHost = "192.168.17.101";
    private int serverPort = 55001;    // 服务器监听的UDP端口
    private int broadcastPort = 55003; // 服务器广播报文的目标端口
    private int localPort = 55005;     // 本地监听的UDP端口
    private int heartBeatInterval = 3000; // 心跳包发送间隔，单位为毫秒

    private String cloudDiskCacheDir = "/sdcard/cloudteachbox/cache";
    private String cloudDiskHost = "192.168.17.101";
    private int cloudDiskPort = 21;

    public String serialNumber = "";
    public String hardwareVersion = "V5.0";
    private String softwareVersion = "1.0.0.1"; // 主版本号，从版本号，次版本号，编译数

    private configures() {
        // 加载配置文件

        // 如果配置文件不存在则使用默认
        // 并把默认值更新到配置文件中

        // 否则加载配置文件内容
    }

    public void reload() {
        // 重新加载配置文件
    }

    public void save() {
        // 保持配置文件
    }

    public int getPowerInputDelays() {
        return powerInputDelays;
    }
    public void setPowerInputDelays(int delays) {
        powerInputDelays = delays;
    }

    public int getPowerOutput0Delays() {
        return powerOutput0Delays;
    }
    public void setPowerOutput0Delays(int delays) {
        this.powerOutput0Delays = delays;
    }

    public int getPowerOutput1Delays() {
        return powerOutput1Delays;
    }
    public void setPowerOutput1Delays(int delays) {
        this.powerOutput1Delays = delays;
    }

    public int getPowerOutput2Delays() {
        return powerOutput2Delays;
    }
    public void setPowerOutput2Delays(int delays) {
        this.powerOutput2Delays = delays;
    }

    public int getPowerOutput3Delays() {
        return powerOutput3Delays;
    }
    public void setPowerOutput3Delays(int delays) {
        this.powerOutput3Delays = delays;
    }

    public int getPowerOutput4Delays() {
        return powerOutput4Delays;
    }
    public void setPowerOutput4Delays(int delays) {
        this.powerOutput4Delays = delays;
    }

    public String getServerHost() {
        return serverHost;
    }
    public int getServerPort() {
        return serverPort;
    }
    public void setServer(String host, int port) {
        serverHost = host;
        serverPort = port;
    }
    public int getBroadcastPort() { return broadcastPort; }
    public void setBroadcastPort(int port) { broadcastPort = port; }
    public int getLocalPort() { return localPort; }
    public int getHeartBeatInterval() { return heartBeatInterval; }

    public String getCloudDiskCacheDir() {
        return cloudDiskCacheDir;
    }
    public String getCloudDiskHost() {
        return cloudDiskHost;
    }
    public int getCloudDiskPort() {
        return cloudDiskPort;
    }
    public void setCloudDisk(String host, int port) {
        cloudDiskHost = host;
        cloudDiskPort = port;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
    public String getHardwareVersion() {
        return hardwareVersion;
    }
    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public String getNoticeUrl() { return noticeUrl; }
    public void setNoticeUrl(String url) { noticeUrl = url; }

    private void initSerialNumber() {
        //MD5(utils.getDeviceID()+seed).toString();
    }
}
